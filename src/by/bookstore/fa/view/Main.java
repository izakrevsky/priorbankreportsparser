package by.bookstore.fa.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import by.bookstore.fa.controller.RecordController;
import by.bookstore.fa.exception.FAException;

public class Main implements ActionListener {

	public Main() {
		initComponents();
	}

	private JFrame viewForm;

	private void initComponents() {
		viewForm = new JFrame("Main Form");
		viewForm.setSize(200, 100);
		viewForm.setVisible(true);
		viewForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton button = new JButton("Start");
		button.setVisible(true);
		button.setLocation(12, 12);
		button.setSize(165, 50);
		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {


				String x = getNewFile();
				System.out.println(x);


					try {
						if (x==null) JOptionPane.showMessageDialog(null, "Can't find file");
						File selectedFile = new File(x);
						Date s = java.util.Calendar.getInstance().getTime();
						DateFormat df = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
						df.setTimeZone(TimeZone.getTimeZone("EAT"));
						String date = df.format(s);
						String outFN = "finances_" + date + ".xls";
						File outFileName = new File(outFN);
						RecordController controller = new RecordController(selectedFile, outFileName);
						controller.doWork();
						JOptionPane.showMessageDialog(null, "Done");
					} catch (FAException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
								"Something Wrong, sorry");
					}

			}

		});
		viewForm.getContentPane().add(button);
		viewForm.getContentPane().add(new JLabel());
	}

	public void actionPerformed(ActionEvent action) {
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Main();
			}
		});
	}

	public static String getNewFile() {
		 List <String> fileList = new ArrayList <String> ();


		 String currentDir = new File(".").getAbsolutePath();
		 File file = new File(currentDir);

       File[] files = file.listFiles();
       boolean isRecord = false;
       for (int fileInList = 0; fileInList < files.length; fileInList++) {
           if (files[fileInList].isFile()) {
        	   String fn = files[fileInList].getName();
        	   fn = fn.substring(fn.lastIndexOf('.') + 1);
        	   if (fn.equals("txt")) {
        		   fileList.add(files[fileInList].getName());
        		   isRecord = true;
        	   }

          	 }
       }
       if (!isRecord) return null;
       Collections.sort(fileList);
       return fileList.get(0);
	}

	}