package by.bookstore.fa.exception;

public class FAException extends Exception {
	private static final long serialVersionUID = -5717718913512013336L;

	public FAException(Exception e) {
		super(e);
	}
}
