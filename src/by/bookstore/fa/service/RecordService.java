package by.bookstore.fa.service;

import java.io.File;
import java.util.List;

import by.bookstore.fa.exception.FAException;
import by.bookstore.fa.model.domain.Record;

public interface RecordService {
	
	public List <Record> parseRecord() throws FAException; 
	public void writeToFile(List <Record> data, File fOut) throws FAException;
	
}
