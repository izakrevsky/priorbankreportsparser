package by.bookstore.fa.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;

import by.bookstore.fa.exception.FAException;
import by.bookstore.fa.model.dao.RecordDAO;
import by.bookstore.fa.model.dao.RecordDAOImpl;
import by.bookstore.fa.model.domain.Record;

public class RecordServiceImpl implements RecordService {
	private final static String START_STRING = "^DocDate=";
	private final static char DELIMETR = '^';
	private final static char DELIMETR_KEY = '=';

	private static final String DATA = "DOCDATE_PARAM";
	private static final String NUM = "NUM_PARAM";
	private static final String OPR = "OPR_PARAM";
	private static final String COD = "COD_PARAM";
	private static final String ACC = "ACC_PARAM";
	private static final String DB = "DB_PARAM";
	private static final String UNN = "UNN_PARAM";
	private static final String KOR= "KOR_PARAM";
	private static final String NAZN= "NAZ_PARAM";
	private static final String DOCID = "DID_PARAM";
	private static final String CRD = "CRED_PARAM";
	private static final String TAG = "TAG_PARAM";
	private static final String CAT = "CAT_PARAM";
	private static final String SUM = "SUM_PARAM";

	private RecordDAO dao;

	public RecordServiceImpl(File f) {
		dao = new RecordDAOImpl(f.toString());
	}

	@Override
	public List <Record> parseRecord() throws FAException {
		List<String> inputStrings = dao.listRecords();
		inputStrings = prepareData(inputStrings);
		Record ans = new Record();
		List <Record> answer = new ArrayList <Record> ();
		int i = 0;
		boolean isFirstString = true;

		ResourceBundle param = ResourceBundle.getBundle("parametrs");

		while (i < inputStrings.size()) {

			String key = inputStrings.get(i)
					.substring(1, inputStrings.get(i).indexOf(DELIMETR_KEY))
					.trim();
			String value = inputStrings
					.get(i)
					.substring(inputStrings.get(i).indexOf(DELIMETR_KEY) + 1,
							inputStrings.get(i).length() - 1).trim();
			if (key.equals(param.getString(DATA))) {
				if (!isFirstString) {
					answer.add(ans);
				} else {
					isFirstString = false;
				}
				ans = new Record();
				ans.setDocDate(value);
			}
			if (key.equals(param.getString(NUM))) {
				ans.setNum(value);
			}
			if (key.equals(param.getString(OPR))) {
				ans.setOpr(value);
			}
			if (key.equals(param.getString(COD))) {
				ans.setCod(value);
			}
			if (key.equals(param.getString(ACC))) {
				ans.setAcc(value);
			}
			if (key.equals(param.getString(DB))) {
				ans.setDb(value);
			}
			if (key.equals(param.getString(UNN))) {
				ans.setRecUNN(value);
			}
			if (key.equals(param.getString(KOR))) {
				ans.setKorName(value);
			}
			if (key.equals(param.getString(NAZN))) {
				ans.setNazn(value);
			}
			if (key.equals(param.getString(DOCID))) {
				ans.setInDocID(value);
			}
			if (key.equals(param.getString(CRD))) {
				ans.setCredit(value);
			}
			i++;
		}
		answer.add(ans);
		return answer;
	}

	@Override
	public void writeToFile(List <Record> data, File fOut) throws FAException {
		try {
			File file = fOut;
			FileOutputStream foStream = new FileOutputStream(file);
			ByteArrayOutputStream oStream = new ByteArrayOutputStream();
			generateXls(oStream, data);
			oStream.writeTo(foStream);
			oStream.close();
			oStream.flush();
			foStream.close();
			foStream.flush();
		} catch (Exception e) {
			throw new FAException(e);
		}
	}

	private void generateXls(ByteArrayOutputStream out, List <Record> data)
			throws FAException {
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();

		HSSFRow row;
		HSSFCell cell;

		ResourceBundle header = ResourceBundle.getBundle("headers");

		row = sheet.createRow(0);
		cell = row.createCell(0, Cell.CELL_TYPE_STRING);
		cell.setCellValue(header.getString(DATA));

		cell = row.createCell(1, Cell.CELL_TYPE_STRING);
		cell.setCellValue(header.getString(NAZN));

		cell = row.createCell(2, Cell.CELL_TYPE_STRING);
		cell.setCellValue(header.getString(TAG));

		cell = row.createCell(3, Cell.CELL_TYPE_STRING);
		cell.setCellValue(header.getString(KOR));

		cell = row.createCell(4, Cell.CELL_TYPE_STRING);
		cell.setCellValue(header.getString(CAT));

		cell = row.createCell(5, Cell.CELL_TYPE_NUMERIC);
		cell.setCellValue(header.getString(SUM));


		if(data != null && data.size() > 0) {
			sheet.setAutoFilter(CellRangeAddress.valueOf("A" + String.valueOf(1) +
					":F" + String.valueOf(data.size())));

			for (int i = 0; i < data.size(); i++) {
				Record newString = data.get(i);
				int rowIndex = i+1;
				row = sheet.createRow(rowIndex);
				cell = row.createCell(0, Cell.CELL_TYPE_STRING);
				cell.setCellValue(newString.getDocDate());

				cell = row.createCell(1, Cell.CELL_TYPE_STRING);
				cell.setCellValue(newString.getNazn());

				cell = row.createCell(2, Cell.CELL_TYPE_STRING);
				cell.setCellValue("");

				cell = row.createCell(3, Cell.CELL_TYPE_STRING);
				cell.setCellValue(newString.getKorName());

				cell = row.createCell(4, Cell.CELL_TYPE_STRING);
				cell.setCellValue("");


				try {
					cell = row.createCell(5, Cell.CELL_TYPE_NUMERIC);
					double x = Double.parseDouble(newString.getCredit());
					double y = Double.parseDouble(newString.getDb());
					cell.setCellValue(x-y);
				}
				catch (Exception e) {
					cell = row.createCell(2, Cell.CELL_TYPE_STRING);
					cell.setCellValue(newString.getOpr());
				}

			}
		}

		try {
			wb.write(out);
		} catch (IOException e) {
			throw new FAException(e);
		}

		try {
			out.close();
		} catch (IOException e) {
			throw new FAException(e);
		}
	}

	private static List<String> prepareData(List<String> l) {
		List<String> result = new ArrayList<String>();
		boolean isStarted = false;
		String tmp;
		int i = 0;
		while (i < l.size() - 1) {
			tmp = l.get(i++);
			if (!isStarted) {
				if (tmp.indexOf(START_STRING) != -1) {
					isStarted = true;
					result.add(tmp);
				}
			} else {
				while (!isLineEnded(tmp)) {
					tmp += l.get(i++);
				}
				result.add(tmp);
			}
		}
		return result;
	}

	private static boolean isLineEnded(String s) {
		return (s.indexOf(DELIMETR, 2) != -1);
	}
}
