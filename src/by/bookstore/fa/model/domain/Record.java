package by.bookstore.fa.model.domain;

public class Record {
	private String docDate;
	private String num;
	private String opr;
	private String cod;
	private String acc;
	private String db;
	private String recUNN;
	private String korName;
	private String nazn;
	private String inDocID;
	private String credit;
	
	public String getDocDate() {
		return docDate;
	}
	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getOpr() {
		return opr;
	}
	public void setOpr(String opr) {
		this.opr = opr;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getAcc() {
		return acc;
	}
	public void setAcc(String acc) {
		this.acc = acc;
	}
	public String getDb() {
		return db;
	}
	public void setDb(String db) {
		this.db = db;
	}
	public String getRecUNN() {
		return recUNN;
	}
	public void setRecUNN(String recUNN) {
		this.recUNN = recUNN;
	}
	public String getKorName() {
		return korName;
	}
	public void setKorName(String korName) {
		this.korName = korName;
	}
	public String getNazn() {
		return nazn;
	}
	public void setNazn(String nazn) {
		this.nazn = nazn;
	}
	public String getInDocID() {
		return inDocID;
	}
	public void setInDocID(String inDocID) {
		this.inDocID = inDocID;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	
}
