package by.bookstore.fa.model.dao;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import by.bookstore.fa.exception.FAException;

public class RecordDAOImpl implements RecordDAO {
	private String filePath;
	
	public RecordDAOImpl(String filePath) {
		this.filePath = filePath;
	}
	
	@Override
	public List<String> listRecords() throws FAException {
		List<String> list = new ArrayList<String>();
		try {
	        FileInputStream fstream = new FileInputStream(filePath);
	        DataInputStream in      = new DataInputStream(fstream);
	        BufferedReader  br      = new BufferedReader (new InputStreamReader(in));
	        String strLine;            
	        while ((strLine = br.readLine()) != null)   {
	        	list.add(strLine); 
	        }       
	        in.close();
		} catch (Exception e) {
			throw new FAException(e);
		}
        return list;     	
	}

}
