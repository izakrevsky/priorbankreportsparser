package by.bookstore.fa.model.dao;

import java.util.List;

import by.bookstore.fa.exception.FAException;

public interface RecordDAO {
	
	public List<String> listRecords() throws FAException;
	
}
