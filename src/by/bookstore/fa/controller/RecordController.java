package by.bookstore.fa.controller;

import java.io.File;
import java.util.List;

import by.bookstore.fa.exception.FAException;
import by.bookstore.fa.model.domain.Record;
import by.bookstore.fa.service.RecordService;
import by.bookstore.fa.service.RecordServiceImpl;

public class RecordController {
	
	private RecordService service;
	private File outF;
	
	public RecordController(File f, File outFile) throws FAException {
		service = new RecordServiceImpl(f);
		outF = outFile;
	}
	
	public void doWork() throws FAException {
		List <Record> record = service.parseRecord();
		service.writeToFile(record, outF);
	}


}